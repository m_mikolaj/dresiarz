#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>

class player : public QWidget {
    Q_OBJECT
public:
    player(QWidget *parent = 0, bool isCpu = false);
    void setSzacun(int s);
    void setKasa(int k);
    void setBmw(bool b);
    void setHaracz(int h);
    void setCurrentTurn(int t);

public slots:
    int getSzacun();
    int getKasa();
    bool getBmw();
    int getHaracz();
    int getCurrentTurn();
    bool getIsCpu();
    void newTurn();

signals:
    void nextTurnSignal();

private:
    int szacun;
    int kasa;
    bool bmw;
    int haracz;
    int currentTurn;
    bool isCpu;
};

#endif // PLAYER_H
