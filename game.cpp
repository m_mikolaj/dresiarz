#include "player.h"
#include "game.h"
#include <QWidget>
#include <QVector>

game::game(QWidget *parent) : QWidget(parent) {

}

game::~game() {

}

void game::startGame(int players, int cpuPlayers) {
    this->playersCount = players + cpuPlayers;
    for(int i = 0; i < players; i++) {
        player *p = new player(0, false);
        this->players.append(p);
    }
    if (cpuPlayers >= 1) {
        for(int j = 0; j < cpuPlayers; j++) {
            player *c = new player(0, true);
            this->players.append(c);
        }
    }
    this->currentPlayer = this->players.first();
    int turn = this->currentPlayer->getCurrentTurn();
    this->currentPlayer->setCurrentTurn(++turn);
    emit this->updateLabelsSignal();
}

void game::nextTurn() {
    int index = this->players.indexOf(this->currentPlayer);
    if (index + 1 >= this->playersCount) {
        index = -1;
    }
    this->currentPlayer = this->players.at(index + 1);
    int turn = this->currentPlayer->getCurrentTurn();
    if (turn + 1 >= 11) {
        emit this->endGameSignal();
        return;
    }
    this->currentPlayer->setCurrentTurn(++turn);
    this->checkBmw();
    this->checkHaracz();
    emit this->updateLabelsSignal();
    if (this->currentPlayer->getIsCpu() != false) {
        this->takeCpuTurn();
    }
}

int game::bmwAction(bool clicked) {
    if (this->currentPlayer->getBmw() == true) {
        return -1;
    }
    this->currentPlayer->setBmw(true);
    emit this->nextTurnSignal();
    return 1;
}

int game::lansAction(bool clicked) {
    int szacun = this->currentPlayer->getSzacun();
    this->currentPlayer->setSzacun(szacun + 1);
    emit this->nextTurnSignal();
    return 1;
}

int game::workAction(bool clicked) {
    int kasa = this->currentPlayer->getKasa();
    this->currentPlayer->setKasa(kasa + 2);
    emit this->nextTurnSignal();
    return 1;
}

int game::haraczAction(bool clicked) {
    int kasa = this->currentPlayer->getKasa();
    int haracz = this->currentPlayer->getHaracz();
    if (kasa >= 4) {
        this->currentPlayer->setHaracz(haracz + 1);
        this->currentPlayer->setKasa(kasa - 4);
    } else {
        return -1;
    }
    emit this->nextTurnSignal();
    return 1;
}

int game::iwanAction(bool clicked) {
    int count = this->playersCount;
    int kasa;
    for(int i = 0; i < count; i++) {
        if (this->players.indexOf(this->currentPlayer) != i) {
            kasa = this->players.at(i)->getKasa();
            if (kasa - 1 < 0) {
                this->players.at(i)->setKasa(0);
            } else {
                this->players.at(i)->setKasa(kasa - 1);
            }
        }
    }
    emit this->nextTurnSignal();
    return 1;
}

void game::checkHaracz() {
    int haracz = this->currentPlayer->getHaracz();
    for(int i = 0; i < haracz; i++) {
        this->currentPlayer->setKasa(this->currentPlayer->getKasa() + 1);
    }
}

void game::checkBmw() {
    if (this->currentPlayer->getBmw() == true) {
        int kasa = this->currentPlayer->getKasa();
        int szacun = this->currentPlayer->getSzacun();
        if (kasa >= 2) {
            if (kasa - 2 < 0) {
                this->currentPlayer->setKasa(0);
            } else {
                this->currentPlayer->setKasa(kasa - 2);
            }
            this->currentPlayer->setSzacun(szacun + 3);
        } else {
            if (szacun - 2 < 0) {
                this->currentPlayer->setSzacun(0);
            } else {
                this->currentPlayer->setSzacun(szacun - 2);
            }
        }
    }
}

void game::takeCpuTurn() {
    int result;
    int random;
    while(result != 1) {
        random = qrand() % 4;
        switch(random) {
        case 0:
            result = this->iwanAction(true);
            break;
        case 1:
            result = this->bmwAction(true);
            break;
        case 2:
            result = this->lansAction(true);
            break;
        case 3:
            result = this->workAction(true);
            break;
        case 4:
            result = this->haraczAction(true);
            break;
        }
    }
}

int game::getPlayersCount() {
    return this->playersCount;
}
