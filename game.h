#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QVector>
#include "player.h"

class game : public QWidget {
     Q_OBJECT
public:
    game(QWidget *parent = 0);
    ~game();
    QVector <player*> players;
    player *currentPlayer;
    int getPlayersCount();
public slots:
    void startGame(int players, int cpuPlayers);
    void nextTurn();
    int lansAction(bool clicked);
    int workAction(bool clicked);
    int haraczAction(bool clicked);
    int bmwAction(bool clicked);
    int iwanAction(bool clicked);
    void checkHaracz();
    void checkBmw();
    void takeCpuTurn();
signals:
    void updateLabelsSignal();
    void nextTurnSignal();
    void endGameSignal();

private:
    int playersCount;
};

#endif // GAME_H
