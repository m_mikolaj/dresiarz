#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "game.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void startGameClicked(bool clicked);
    void updateLabels();
    void endGame();
signals:
    void startGameSignal(int players, int cpuPlayers);

private:
    Ui::MainWindow *ui;
    game *gameObj;
};

#endif // MAINWINDOW_H
