#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "game.h"
#include "player.h"
#include <QMessageBox>
#include <QAbstractButton>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    gameObj(new game(this)),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    ui->playersSpinBox->setMinimum(1);
    ui->playersSpinBox->setMaximum(4);
    ui->cpuSpinBox->setMinimum(0);
    ui->cpuSpinBox->setMaximum(4);

    ui->haraczButton->setEnabled(false);

    player *playerObj = new player();

    connect(ui->startButton, SIGNAL(clicked(bool)), this, SLOT(startGameClicked(bool)));
    connect(this, SIGNAL(startGameSignal(int,int)), gameObj, SLOT(startGame(int,int)));
    connect(gameObj, SIGNAL(updateLabelsSignal()), this, SLOT(updateLabels()));
    connect(gameObj, SIGNAL(nextTurnSignal()), gameObj, SLOT(nextTurn()));
    connect(ui->bmwButton, SIGNAL(clicked(bool)), gameObj, SLOT(bmwAction(bool)));
    connect(ui->lansButton, SIGNAL(clicked(bool)), gameObj, SLOT(lansAction(bool)));
    connect(ui->haraczButton, SIGNAL(clicked(bool)), gameObj, SLOT(haraczAction(bool)));
    connect(ui->workButton, SIGNAL(clicked(bool)), gameObj, SLOT(workAction(bool)));
    connect(ui->iwanButton, SIGNAL(clicked(bool)), gameObj, SLOT(iwanAction(bool)));
    connect(gameObj, SIGNAL(endGameSignal()), this, SLOT(endGame()));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::startGameClicked(bool clicked) {
    emit this->startGameSignal(ui->playersSpinBox->value(), ui->cpuSpinBox->value());
}

void MainWindow::updateLabels() {
    QString player = QString::number(gameObj->players.indexOf(gameObj->currentPlayer) + 1);
    ui->playerLabel->setText(player);
    QString turn = QString::number(gameObj->currentPlayer->getCurrentTurn());
    ui->turnLabel->setText(turn);
    QString haracz = QString::number(gameObj->currentPlayer->getHaracz());
    ui->haraczLabel->setText(haracz);
    QString szacun = QString::number(gameObj->currentPlayer->getSzacun());
    ui->szacunLabel->setText(szacun);
    QString kasa = QString::number(gameObj->currentPlayer->getKasa());
    ui->kasaLabel->setText(kasa);
    QString bmw = QString::number(gameObj->currentPlayer->getBmw());
    ui->bmwLabel->setText(bmw);

    ui->haraczButton->setEnabled(false);
    ui->bmwButton->setEnabled(true);

    if (ui->kasaLabel->text().toInt() >= 4) {
        ui->haraczButton->setEnabled(true);
    }
    if (ui->bmwLabel->text().toInt() == 1) {
        ui->bmwButton->setEnabled(false);
    }
}

void MainWindow::endGame() {
    QMessageBox box;
    QAbstractButton *okButton =
          box.addButton(tr("Ok"), QMessageBox::ActionRole);
    int maxSzacun = 0;
    int szacun;
    int player = 0;
    int playersCount = gameObj->getPlayersCount();
    for(int i = 0; i < playersCount; i++) {
        szacun = gameObj->players.at(i)->getSzacun();
        if (szacun > maxSzacun) {
            maxSzacun = szacun;
            player = i;
        }
    }
    box.setText("The winner is player " + QString::number(player + 1) + " with " + QString::number(maxSzacun) + " szacun.");
    box.exec();
    connect(okButton, SIGNAL(clicked(bool)), this, SLOT(close()));
}


