#-------------------------------------------------
#
# Project created by QtCreator 2016-10-06T20:24:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dresiarz
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    game.cpp \
    player.cpp

HEADERS  += mainwindow.h \
    game.h \
    player.h

FORMS    += mainwindow.ui
