#include "player.h"
#include <QWidget>

player::player(QWidget *parent, bool isCpu) : QWidget(parent) {
    this->bmw = false;
    this->haracz = 0;
    this->kasa = 0;
    this->szacun = 0;
    this->currentTurn = 0;
    this->isCpu = isCpu;
}

bool player::getBmw() {
    return this->bmw;
}

int player::getHaracz() {
    return this->haracz;
}

int player::getKasa() {
    return this->kasa;
}

int player::getSzacun() {
    return this->szacun;
}

int player::getCurrentTurn() {
    return this->currentTurn;
}

void player::setBmw(bool b) {
    this->bmw = b;
}

void player::setHaracz(int h) {
    this->haracz = h;
}

void player::setKasa(int k) {
    this->kasa = k;
}

void player::setSzacun(int s) {
    this->szacun = s;
}

void player::setCurrentTurn(int t) {
    this->currentTurn = t;
}

void player::newTurn() {
    this->currentTurn += 1;
}

bool player::getIsCpu() {
    return this->isCpu;
}
